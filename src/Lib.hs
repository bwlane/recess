module Lib where

someFunc :: IO ()
someFunc = putStrLn "someFunc"

anotherFunc :: IO ()
anotherFunc = putStrLn "anotherFunc"

badFunc :: String
badFunc = "Hello"