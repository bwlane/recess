{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import Data.Aeson
import Data.Generics
import Data.HashMap.Strict
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Time (defaultTimeLocale, formatTime, getZonedTime)
import Data.Typeable (typeOf)
import GHC.Exts
import GHC.Generics as G
import Options.Applicative
  ( Parser,
    auto,
    execParser,
    fullDesc,
    header,
    help,
    helper,
    info,
    long,
    metavar,
    option,
    progDesc,
    short,
    showDefault,
    strOption,
    switch,
    value,
    (<**>),
  )
import System.Directory (createDirectory, createDirectoryIfMissing, getCurrentDirectory)
import System.Environment (getArgs)
import System.FilePath.Posix ((</>))
import Text.Replace

main :: IO ()
main = do
  let f = Flag "dd/dd/dd" MacOS [""] "code" "https:www.example.com" True True True
  let j = encode f
  let hmap = decode j :: Maybe (HashMap Text Value)
  case hmap of 
    Just a -> a >>= mapM_ $ \k v 
    Nothing -> putStrLn "No value"


toShow:: HashMap -> Show -> HashMap
-- main = buildPlayground =<< execParser opts
--   where
--     opts =
--       info
--         (parseFlags <**> helper)
--         ( fullDesc
--             <> progDesc "Create a new playground at TARGETPATH for PLATFORM"
--             <> header "🏫 recess - a command-line tool to create Xcode Playgrounds"
--         )

-- data structures

data Platform = MacOS | IOS deriving (Data, Read, Show, G.Generic)

instance ToJSON Platform

instance FromJSON Platform

data Flag = Flag
  { targetPath :: String,
    platform :: Platform,
    dependencies :: [String],
    code :: String,
    url :: String,
    addViewCode :: Bool,
    forceOverwrite :: Bool,
    autoRun :: Bool
  }
  deriving (Data, Read, Show, G.Generic)

instance ToJSON Flag where
  toEncoding = genericToEncoding defaultOptions

instance FromJSON Flag

xcContentsReplace :: Flag -> Text -> Text
xcContentsReplace f s = undefined

flagToList :: Flag -> [Text]
flagToList f = Prelude.map wrapFlag $ getNames f

-- Get field names (empty list if not record constructor)
getNames :: Data object => object -> [Text]
getNames = Prelude.map Text.pack . constrFields . toConstr

wrapFlag :: Text -> Text
wrapFlag t = "${{" <> t <> "}}"

-- Parser

parseFlags :: Parser Flag
parseFlags =
  Flag
    <$> strOption
      ( long "targetPath"
          <> short 't'
          <> metavar "TARGETPATH"
          <> showDefault
          <> value "~/Desktop/"
          <> help " Specify a target path where the playground should be created\nDefault: ~/Desktop/<Date>"
      )
      <*> option
        auto
        ( long "platform"
            <> short 'p'
            <> metavar "PLATFORM"
            <> showDefault
            <> value MacOS
            <> help "Select platform (iOS, macOS or tvOS) that the playground should run on \nDefault: macOS"
        )
      <*> option
        auto
        ( long "dependencies"
            <> short 'd'
            <> metavar "DEPENDENCIES"
            <> showDefault
            <> value []
            <> help "Specify any Xcode projects that you wish to add as dependencies\nShould be a comma-separated list of file paths"
        )
      <*> strOption
        ( long "code"
            <> short 'c'
            <> metavar "CODE"
            <> showDefault
            <> value "import Foundation\n let str = \"Hello World\""
            <> help "Any code that you want the playground to contain\nPass this flag without a value to use the contents of your clipboard\nDefault: An empty playground that imports the system framework"
        )
      <*> strOption
        ( long "url"
            <> short 'u'
            <> metavar "URL"
            <> showDefault
            <> value ""
            <> help "Any URL to code that you want the playground to contain\nGist & GitHub links are automatically handled"
        )
      <*> switch
        ( long "addViewCode"
            <> short 'v'
            <> help "Fill the playground with the code required to prototype a view\nDefault: Any code specified with -c or its default value"
        )
      <*> switch
        ( long "forceOverwrite"
            <> short 'f'
            <> help "Force overwrite any existing playground at the target path\nDefault: Don't overwrite, and instead open any existing playground"
        )
      <*> switch
        ( long "autoRun"
            <> short 'a'
            <> help "Turn on auto run for the generated playground\nDefault: False"
        )

getFormattedDate = do formatTime defaultTimeLocale "%Y-%m-%d-%I%m-%S" <$> getZonedTime

xcPlaygroundContentsPath :: FilePath
xcPlaygroundContentsPath = "./config/xcplaygroundcontents"

buildPlayground :: Flag -> IO ()
buildPlayground flag = do
  now <- getFormattedDate -- (A) why use an arrow here? IO Monad?
  putStrLn ("Now will be " ++ now)
  let interimPath = if targetPath flag == "." then "" else targetPath flag
  let targetDir = interimPath <> now <> ".playground" -- (B) but a let here?
  putStrLn targetDir
  createDirectoryIfMissing True targetDir
  writeFile (targetDir </> "Contents.swift") "import Foundation\nlet str = \"Hello World\""
  contents <- readFile xcPlaygroundContentsPath
  writeFile (targetDir </> "contents.xcplayground") contents

-- What does the program do?
-- Imperative vs. Declarative
-- Declarative:
-- DS that has all the info that can do the side effects
-- Imp:
-- Step by step does the work that produces the side effects

-- write a playground to disk: requires a path, runs in IO monad
-- write file function may exist
-- writeFile :: FilePath -> String -> IO () <-- this exists!